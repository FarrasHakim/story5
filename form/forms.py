from django import forms
from .models import Agenda


class TheForm(forms.ModelForm):
    class Meta:
        model = Agenda
        fields = '__all__'
        widgets = {
            'agenda': forms.TextInput(attrs={'class': 'form-control col-sm-7', 'placeholder': 'Your Agenda'}),
            'kategori': forms.TextInput(attrs={'class': 'form-control col-sm-7', 'placeholder': 'Agenda Category'}),
            'tempat': forms.TextInput(attrs={'class': 'form-control col-sm-7', 'placeholder': 'Location'}),
            'tanggal': forms.DateInput(attrs={'class': 'form-control col-sm-7', 'placeholder': 'yyyy-mm-dd', 'type': 'date'}),
            'waktu': forms.TimeInput(attrs={'class': 'form-control col-sm-7', 'placeholder': 'hh:mm:ss', 'type': 'time'}),
        }
        labels = {
            'agenda': 'Agenda',
            'kategori': 'Kategori',
            'tempat': 'Tempat',
            'tanggal': 'Tanggal',
            'waktu': 'Waktu',
        }
