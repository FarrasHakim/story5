from django.db import models


class Agenda(models.Model):
    agenda = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50)
    tempat = models.CharField(max_length=50)
    tanggal = models.DateField()
    waktu = models.TimeField()
