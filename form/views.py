from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import TheForm
from .models import Agenda


def Form(request):
    form = TheForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            print(form.cleaned_data)
            form.save()
            return HttpResponseRedirect('/form/jadwal/')
        else:
            print(form.errors)
    context = {
        'form': form,
    }
    return render(request, 'newform.html', context)


def Jadwal(request):
    agendas = Agenda.objects.all()
    context = {
        'agenda': agendas
    }
    return render(request, 'jadwal.html', context)


def delete_all(request):
    Agenda.objects.all().delete()
    return render(request, 'jadwal.html')
