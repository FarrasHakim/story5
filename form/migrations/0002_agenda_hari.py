# Generated by Django 2.1.1 on 2018-10-03 12:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('form', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='agenda',
            name='hari',
            field=models.CharField(default='Senin', max_length=8),
        ),
    ]
