from django.urls import path

from form import views

urlpatterns = [
    path('jadwal/', views.Jadwal, name='jadwal'),
    path('', views.Form, name='form'),
    path('jadwal/delete_dis', views.delete_all, name='delet')
]
